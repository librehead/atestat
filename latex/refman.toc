\babel@toc {romanian}{}
\contentsline {chapter}{\numberline {1}Omida}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Cum se joaca?}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Meniu start}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Jocul efectiv}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Pauza}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}Final}{6}{section.2.4}
\contentsline {chapter}{\numberline {3}Cum functioneaza?}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Desenand pe ecran}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Cum reprezint obiectele jocului?}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}Meniuri}{10}{section.3.3}
\contentsline {section}{\numberline {3.4}Principala repetitie}{11}{section.3.4}
\contentsline {chapter}{\numberline {4}La ce ajuta?}{13}{chapter.4}
\contentsline {chapter}{\numberline {5}Extra}{15}{chapter.5}
\contentsline {chapter}{\numberline {6}Indexul Namespace-\/ului}{17}{chapter.6}
\contentsline {section}{\numberline {6.1}Lista de Namespace-\/uri}{17}{section.6.1}
\contentsline {chapter}{\numberline {7}Index Ierarhic}{19}{chapter.7}
\contentsline {section}{\numberline {7.1}Ierarhia Claselor}{19}{section.7.1}
\contentsline {chapter}{\numberline {8}Indexul Claselor}{21}{chapter.8}
\contentsline {section}{\numberline {8.1}Lista Claselor}{21}{section.8.1}
\contentsline {chapter}{\numberline {9}Indexul Fi\IeC {\c s}ierelor}{23}{chapter.9}
\contentsline {section}{\numberline {9.1}Lista fi\IeC {\c s}ierelor}{23}{section.9.1}
\contentsline {chapter}{\numberline {10}Documenta\IeC {\c t}ia Namespace-\/ului}{25}{chapter.10}
\contentsline {section}{\numberline {10.1}Referin\IeC {\c t}\IeC {\u a} la Namespace-\/ul global}{25}{section.10.1}
\contentsline {subsection}{\numberline {10.1.1}Descriere Detaliat\IeC {\u a}}{25}{subsection.10.1.1}
\contentsline {subsection}{\numberline {10.1.2}Documenta\IeC {\c t}ia variabilelor}{25}{subsection.10.1.2}
\contentsline {subsubsection}{\numberline {10.1.2.1}desenator}{25}{subsubsection.10.1.2.1}
\contentsline {subsubsection}{\numberline {10.1.2.2}fereastra\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}inchisa}{26}{subsubsection.10.1.2.2}
\contentsline {subsubsection}{\numberline {10.1.2.3}scene}{26}{subsubsection.10.1.2.3}
\contentsline {subsubsection}{\numberline {10.1.2.4}taste\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}apasate}{26}{subsubsection.10.1.2.4}
\contentsline {chapter}{\numberline {11}Documenta\IeC {\c t}ia Claselor}{27}{chapter.11}
\contentsline {section}{\numberline {11.1}Referin\IeC {\c t}\IeC {\u a} la clasa Joc}{27}{section.11.1}
\contentsline {subsection}{\numberline {11.1.1}Descriere Detaliat\IeC {\u a}}{28}{subsection.11.1.1}
\contentsline {subsection}{\numberline {11.1.2}Documenta\IeC {\c t}ia Func\IeC {\c t}iilor Membre}{28}{subsection.11.1.2}
\contentsline {subsubsection}{\numberline {11.1.2.1}incarca()}{29}{subsubsection.11.1.2.1}
\contentsline {subsubsection}{\numberline {11.1.2.2}incarcat\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}deja()}{29}{subsubsection.11.1.2.2}
\contentsline {subsubsection}{\numberline {11.1.2.3}itereaza()}{29}{subsubsection.11.1.2.3}
\contentsline {subsubsection}{\numberline {11.1.2.4}muta\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}omida()}{30}{subsubsection.11.1.2.4}
\contentsline {subsubsection}{\numberline {11.1.2.5}pauza()}{30}{subsubsection.11.1.2.5}
\contentsline {subsubsection}{\numberline {11.1.2.6}verifica\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}taste\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}apasate()}{30}{subsubsection.11.1.2.6}
\contentsline {subsection}{\numberline {11.1.3}Documenta\IeC {\c t}ia Datelor Membre}{31}{subsection.11.1.3}
\contentsline {subsubsection}{\numberline {11.1.3.1}m\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}timp\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}asteptat}{31}{subsubsection.11.1.3.1}
\contentsline {subsubsection}{\numberline {11.1.3.2}noua\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}directie\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}a\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}omizii}{31}{subsubsection.11.1.3.2}
\contentsline {section}{\numberline {11.2}Referin\IeC {\c t}\IeC {\u a} la clasa Meniu\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Final}{31}{section.11.2}
\contentsline {subsection}{\numberline {11.2.1}Descriere Detaliat\IeC {\u a}}{32}{subsection.11.2.1}
\contentsline {subsection}{\numberline {11.2.2}Documenta\IeC {\c t}ia Func\IeC {\c t}iilor Membre}{33}{subsection.11.2.2}
\contentsline {subsubsection}{\numberline {11.2.2.1}incarca()}{33}{subsubsection.11.2.2.1}
\contentsline {subsubsection}{\numberline {11.2.2.2}incarcat\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}deja()}{33}{subsubsection.11.2.2.2}
\contentsline {subsubsection}{\numberline {11.2.2.3}itereaza()}{33}{subsubsection.11.2.2.3}
\contentsline {subsection}{\numberline {11.2.3}Documenta\IeC {\c t}ia Datelor Membre}{33}{subsection.11.2.3}
\contentsline {subsubsection}{\numberline {11.2.3.1}m\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}istoric}{34}{subsubsection.11.2.3.1}
\contentsline {section}{\numberline {11.3}Referin\IeC {\c t}\IeC {\u a} la clasa Meniu\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Start}{34}{section.11.3}
\contentsline {subsection}{\numberline {11.3.1}Descriere Detaliat\IeC {\u a}}{35}{subsection.11.3.1}
\contentsline {subsection}{\numberline {11.3.2}Documenta\IeC {\c t}ia Func\IeC {\c t}iilor Membre}{35}{subsection.11.3.2}
\contentsline {subsubsection}{\numberline {11.3.2.1}incarca()}{35}{subsubsection.11.3.2.1}
\contentsline {subsubsection}{\numberline {11.3.2.2}incarcat\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}deja()}{36}{subsubsection.11.3.2.2}
\contentsline {subsubsection}{\numberline {11.3.2.3}itereaza()}{36}{subsubsection.11.3.2.3}
\contentsline {section}{\numberline {11.4}Referin\IeC {\c t}\IeC {\u a} la structura Punct}{36}{section.11.4}
\contentsline {subsection}{\numberline {11.4.1}Descriere Detaliat\IeC {\u a}}{37}{subsection.11.4.1}
\contentsline {subsection}{\numberline {11.4.2}Documenta\IeC {\c t}ia pentru Constructori \IeC {\c s}i Destructori}{37}{subsection.11.4.2}
\contentsline {subsubsection}{\numberline {11.4.2.1}Punct()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/3]}}{37}{subsubsection.11.4.2.1}
\contentsline {subsubsection}{\numberline {11.4.2.2}Punct()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/3]}}{37}{subsubsection.11.4.2.2}
\contentsline {subsubsection}{\numberline {11.4.2.3}Punct()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [3/3]}}{38}{subsubsection.11.4.2.3}
\contentsline {section}{\numberline {11.5}Referin\IeC {\c t}\IeC {\u a} la clasa Scena}{38}{section.11.5}
\contentsline {subsection}{\numberline {11.5.1}Descriere Detaliat\IeC {\u a}}{38}{subsection.11.5.1}
\contentsline {subsection}{\numberline {11.5.2}Documenta\IeC {\c t}ia Func\IeC {\c t}iilor Membre}{39}{subsection.11.5.2}
\contentsline {subsubsection}{\numberline {11.5.2.1}incarca()}{39}{subsubsection.11.5.2.1}
\contentsline {subsubsection}{\numberline {11.5.2.2}incarcat\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}deja()}{39}{subsubsection.11.5.2.2}
\contentsline {subsubsection}{\numberline {11.5.2.3}itereaza()}{39}{subsubsection.11.5.2.3}
\contentsline {section}{\numberline {11.6}Referin\IeC {\c t}\IeC {\u a} la clasa Stadiul\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Jocului}{40}{section.11.6}
\contentsline {subsection}{\numberline {11.6.1}Descriere Detaliat\IeC {\u a}}{41}{subsection.11.6.1}
\contentsline {subsection}{\numberline {11.6.2}Documenta\IeC {\c t}ia Datelor Membre}{41}{subsection.11.6.2}
\contentsline {subsubsection}{\numberline {11.6.2.1}directia\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}omizii}{41}{subsubsection.11.6.2.1}
\contentsline {subsubsection}{\numberline {11.6.2.2}lungime\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}textura}{41}{subsubsection.11.6.2.2}
\contentsline {subsubsection}{\numberline {11.6.2.3}pozitie\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}powerup}{42}{subsubsection.11.6.2.3}
\contentsline {subsubsection}{\numberline {11.6.2.4}pozitii\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}omida}{42}{subsubsection.11.6.2.4}
\contentsline {subsubsection}{\numberline {11.6.2.5}scor}{42}{subsubsection.11.6.2.5}
\contentsline {subsubsection}{\numberline {11.6.2.6}teren}{42}{subsubsection.11.6.2.6}
\contentsline {chapter}{\numberline {12}Documenta\IeC {\c t}ia Fi\IeC {\c s}ierelor}{43}{chapter.12}
\contentsline {section}{\numberline {12.1}Referin\IeC {\c t}\IeC {\u a} la fi\IeC {\c s}ierul main.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp}{43}{section.12.1}
\contentsline {subsection}{\numberline {12.1.1}Documenta\IeC {\c t}ia enumer\IeC {\u a}rilor}{45}{subsection.12.1.1}
\contentsline {subsubsection}{\numberline {12.1.1.1}Posibilitate\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Teren}{45}{subsubsection.12.1.1.1}
\contentsline {subsection}{\numberline {12.1.2}Documenta\IeC {\c t}ia func\IeC {\c t}iilor}{45}{subsection.12.1.2}
\contentsline {subsubsection}{\numberline {12.1.2.1}deseneaza\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cap()}{45}{subsubsection.12.1.2.1}
\contentsline {subsubsection}{\numberline {12.1.2.2}deseneaza\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}omida()}{45}{subsubsection.12.1.2.2}
\contentsline {subsubsection}{\numberline {12.1.2.3}genereaza\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}pozitie\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}noua()}{46}{subsubsection.12.1.2.3}
\contentsline {subsubsection}{\numberline {12.1.2.4}initializeaza()}{46}{subsubsection.12.1.2.4}
\contentsline {subsubsection}{\numberline {12.1.2.5}main()}{46}{subsubsection.12.1.2.5}
\contentsline {subsubsection}{\numberline {12.1.2.6}marcheaza\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}frunza()}{47}{subsubsection.12.1.2.6}
\contentsline {subsection}{\numberline {12.1.3}Documenta\IeC {\c t}ia variabilelor}{47}{subsection.12.1.3}
\contentsline {subsubsection}{\numberline {12.1.3.1}directie}{47}{subsubsection.12.1.3.1}
\contentsline {chapter}{Index}{49}{section*.32}
