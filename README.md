# Introducere

"Omida" este un mic proiect pentru atestatul meu la informatica. Este un joc care seamana foarte tare cu snake dar nu l-am putut numi snake pentru ca numele era deja luat :(.

# Documentatie
https://alexandruica.github.io/Atestat-documentatie/

# Showoff

Jocul arata cam asa:

![Alt text](./IMG/demo.gif?raw=true "Demo")
